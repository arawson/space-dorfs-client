

import * as mongoose from 'mongoose';

export interface IUser {
	email?: string;
	firstName?: string;
	lastName?: string;
}
