import { TestBed, inject } from '@angular/core/testing';

import { GrabberService } from './grabber.service';

describe('GrabberService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GrabberService]
    });
  });

  it('should be created', inject([GrabberService], (service: GrabberService) => {
    expect(service).toBeTruthy();
  }));
});
