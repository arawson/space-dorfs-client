import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GrabberService {

  constructor(
      private http: HttpClient
  ) { }

  private grabUrl = 'api';

  public getGrab() : void {
        //   this.http.get(this.grabUrl).pipe(
        //       catchError(this.handleError('grab', []))
        //   ).forEach()
        this.http.get(this.grabUrl).forEach(
            console.log
        );
  }

  private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
          console.error(error);
          return of(result as T);
      };
  }
}
