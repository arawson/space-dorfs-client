import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrabApiComponent } from './grab-api.component';

describe('GrabApiComponent', () => {
  let component: GrabApiComponent;
  let fixture: ComponentFixture<GrabApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrabApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrabApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
