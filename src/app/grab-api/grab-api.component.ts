import { Component, OnInit } from '@angular/core';
import { GrabberService } from '../services/grabber.service';

@Component({
  selector: 'app-grab-api',
  templateUrl: './grab-api.component.html',
  styleUrls: ['./grab-api.component.css']
})
export class GrabApiComponent implements OnInit {

  constructor(
      private grabber: GrabberService
  ) { }

  ngOnInit() {
  }

  doGrab(): void {
      this.grabber.getGrab();
  }

}
