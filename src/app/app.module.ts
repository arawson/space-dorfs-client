import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { CallbackComponentComponent } from './callback-component/callback-component.component';
import { GrabApiComponent } from './grab-api/grab-api.component';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClient, HttpClientModule } from '../../node_modules/@angular/common/http';

const routes: Routes = [
    {path: "callback", component: CallbackComponentComponent},
    {path: "grab-api", component: GrabApiComponent},
    // {path: "home", redirectTo: "/"}
];

export function tokenGetter() {
    return localStorage.getItem('id_token');
}

@NgModule({
  declarations: [
    AppComponent,
    CallbackComponentComponent,
    GrabApiComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AppRoutingModule,
    JwtModule.forRoot({
        config: {
            tokenGetter: tokenGetter
            // blacklistedRoutes: [
            //     'callback'
            // ]
            // whitelistedDomains: []
        }
    }),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
